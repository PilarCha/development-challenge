import { render, waitFor, screen, within } from "@testing-library/react";
import App from "./App";

test("table renders with headers", async () => {
  render(<App />);
  await waitFor(() => screen.getByRole("table"));
  const uuidHeader = screen.getByText(/Uuid/g);
  expect(uuidHeader).toBeInTheDocument();
  const nameHeader = screen.getByText(/Name/g);
  expect(nameHeader).toBeInTheDocument();
  const emailHeader = screen.getByText(/Email/g);
  expect(emailHeader).toBeInTheDocument();
  const requestedAmountHeader = screen.getByText(/Requested Amount/g);
  expect(requestedAmountHeader).toBeInTheDocument();
  const paymentAmountHeader = screen.getByText(/Payment Amount/g);
  expect(paymentAmountHeader).toBeInTheDocument();
  const paymentMethodHeader = screen.getByText(/Payment Method/g);
  expect(paymentMethodHeader).toBeInTheDocument();
  const initiatePaymentHeader = screen.getByText(/Initiate Payment/g);
  expect(initiatePaymentHeader).toBeInTheDocument();
});

describe("Pay Button", () => {
  it("should render if user has Requested amount and Payment Amount is empty", async () => {
    render(<App />);
    await screen.findByRole("table");
    const rows = screen.getAllByRole("row");
    for (let i = 1; i < 10; i++) {
      const row = rows[i];
      const columns = row.querySelectorAll("td");
      if (columns[3].innerHTML !== "" && columns[4].innerHTML === "") {
        expect(row).toContainElement(within(row).queryByRole("button"));
      } else {
        expect(row).not.toContainElement(within(row).queryByRole("button"));
      }
    }
  });
});
